import os
import time
import typer
from .build_module import BuildModule


app = typer.Typer()


@app.command()
def build(module_path: str):
    """
    Shoot the portal gun
    """
    base_path = os.path.dirname(module_path)
    module_name = os.path.basename(module_path)
    module = BuildModule(base_path, module_name)
    print("total dependencies: {}".format(len(module.dependencies)))
    module.build_dependency_graph()
    module.build()




if __name__ == "__main__":
    app()
