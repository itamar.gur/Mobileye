import os
from time import sleep
import networkx as nx
import json


class BuildModule:
    
    def __init__(self, base_path: str, module_name: str):

        path = os.path.join(base_path, module_name, "module.json")

        if not os.path.isfile(path):
            raise Exception("module not found")
        

        self.base_path = base_path
        with open(path, 'r') as json_file:
            module_dict = json.load(json_file)
            self.name = module_dict["name"]
            self.dependencies = module_dict["dependencies"]

    
    def build_dependency_graph(self):
        self.dependency_graph = nx.DiGraph()
        self.dependency_graph.add_node(self.name)
        self.__build_graph(module_name=self.name)

    def build(self):
        cycle = self.__validate_dependency_graph()

        if len(cycle) > 0:
            raise CycleDependencyError("module have cycle dependencies: {}".format(cycle))
        else:
            dependecies_list = self.__walk_dependency_graph()
            print(str(dependecies_list))

    
    def __build_graph(self, module_name):
         
        if len(self.dependency_graph.out_edges(module_name)) > 0:
            return

        if module_name != self.name:
            module = BuildModule(self.base_path, module_name)
        else:
            module = self

        for dependency in module.dependencies:
            self.dependency_graph.add_node(dependency)
            self.dependency_graph.add_edge(module_name, dependency)
            self.__build_graph(module_name=dependency)
    
    def __validate_dependency_graph(self) -> list:
        cycle = []
        try:
            cycle = nx.find_cycle(self.dependency_graph)
        except nx.NetworkXNoCycle:
            pass
        finally:
            return cycle
    

    def __walk_dependency_graph(self) -> list:
        return list(reversed(list(nx.topological_sort(self.dependency_graph))))




class CycleDependencyError(Exception):
    pass
