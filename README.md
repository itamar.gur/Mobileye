# Mobileye Build Test


1. clone this project

2. cd to project root directory

3. install the wheel file

```sh
pip install --user dist/builder-0.1.0-py3-none-any.whl
```
* use 'pip3' if using python3


4. cd to test/modules and run:

```sh
builder <module name>
```





